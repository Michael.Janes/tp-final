document.addEventListener("DOMContentLoaded", function () {



    //

    var connexion = new MovieDb();

    if(document.location.pathname.search("fiche-film.html")>0)
    {
        var params = (new URL(document.location) ).searchParams;

        connexion.requeteFilm(params.get("id"));
        connexion.requeteTopRated(params.get("id"));

    }
    else {
        connexion.getDernierFilm();
        connexion.requeteTopRated();

    }


    document.querySelector("#retour").addEventListener("click", top);

    function top() {
        window.scrollTo({top:0, behavior:'smooth'});
    }


    var Swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });




    document.querySelector("#connexion").addEventListener("click", function () {
        var modale = document.querySelector(".login-container-cache");

        modale.classList.add("login-container");
        modale.classList.remove("login-container-cache");

    });

    document.querySelector("#fermer").addEventListener("click", function () {
        var modale = document.querySelector(".login-container");

        modale.classList.remove("login-container");
        modale.classList.add("login-container-cache");

    });




});




function reduireTexte (element, nbrMaxMot, sufix) {


    // Nous vérifions si les arguments "element" et "maxChar"
    // sont bien passé dans l'appel de la fonction.
    if (!element || !nbrMaxMot) return;

    // Nous récupèrons le texte de l'élément
    var texte = element.textContent;

    // Nous convertisons la chaine de caractères en tableau avec .split(' ').
    // Le split utilise un espace, ce qui va donner un tableau
    // avec chaque mot dans une cellule.
    texte = texte.split(' ');

    // Nous arrêtons la fonction s'il y a moins de mots que nbrMaxMot
    if(texte.length < nbrMaxMot) return;

    // Nous coupons le tableau avec .slice(0, maxChar)
    // pour récupérer seulement le nombre de mots voulu.
    texte = texte.slice(0, nbrMaxMot);

    // Nous recréons une chaine de caractère avec .join(' ').
    // Le join utilise un espace pour séparer chaque mot.
    // S'il y a un sufix, nous ajoutons le sufix à la fin du texte.
    texte = texte.join(' ') + (sufix ? sufix : '');

    // Nous réinjectons le texte dans l'élément.
    element.textContent = texte;

};





class MovieDb {

    constructor() {

        this.APIkey = "34cca7090fdda59238f08b655f25faa7";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;

    }

    requeteTopRated() {

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteTopRated.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    retourRequeteTopRated(e) {

        let target = e.currentTarget;  //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficheTopRated(data);

            //console.log(data[2].title);
        }
    }

    afficheTopRated(data){

        for (var i = 0; i < this.totalFilm; i++){
            // console.log(data[i].title);

            let unArticle = document.querySelector(".template").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;

            if (data[i].overview == ""){
                unArticle.querySelector("p").innerText = "Pas de descriptions";
            }
            else{
                unArticle.querySelector("p").innerText = data[i].overview;
                reduireTexte(unArticle.querySelector("p"), 20, '...');
            }
            var date = unArticle.querySelector("#date").innerHTML = data[i].release_date;

            var uneImage = unArticle.querySelector("img").setAttribute("src",this.imgPath + "w" + this.largeurAffiche[5] + data[i].poster_path);

           var etoile = unArticle.querySelector("#etoile").innerHTML = data[i].vote_average/2 +"/5 Étoiles";



            document.querySelector(".liste-films").appendChild(unArticle);



        }
    }



    getDernierFilm(){

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourGetDernierFilm.bind(this));

        xhr.open("GET", this.baseURL+"movie/now_playing?language="+this.lang+"&api_key="+this.APIkey);

        xhr.send();
    }

    retourGetDernierFilm(e){

        let target = e.currentTarget;

        var data;

        if (target.readyState === target .DONE) {

            data =  JSON.parse(target.responseText).results;

            this.afficherDernierFilm(data);
        }
    }

    afficherDernierFilm(data){
        for (let i=0; i<9;i++) {
            // Clone
            let objet = document.querySelector("#toCloneSwiper").cloneNode(true);

            objet.setAttribute("href", "html/fiche-film.html?id="+data[i].id);
            objet.classList.remove("u-hidden");

            objet.querySelector("h3").innerHTML = data[i].title;
            objet.querySelector("p").innerHTML = data[i].vote_average/2 + " Étoiles sur 5";


            objet.querySelector("article").style.backgroundImage = "url('"+this.imageLien+"original"+data[i].backdrop_path+"')";




        }



    }





    requeteFilm(id){

        let xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteFilm.bind(this));

        xhr.open("GET", this.baseURL+"movie/"+id+"?language="+this.lang+"&api_key="+this.APIkey);

        xhr.send();
    }

    retourRequeteFilm(e){

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {


            data =  JSON.parse(target.responseText);

            this.afficherFilm(data);
        }
    }

    afficherFilm(data){

        console.log(data);

        document.querySelector("hero").style.backgroundImage = "url('"+this.imageLien+"original"+data.backdrop_path+"')";

        let article = document.querySelector("article");

        article.querySelector("h1").innerHTML = data.title;

        article.querySelector("p.date").innerHTML = data.release_date +" "+data.original_language;
        article.querySelector("p.etoile").innerHTML = data.vote_average/2 +"/5 Étoiles";

        article.querySelector("p.duree").innerHTML = data.runtime + " minutes";
        article.querySelector("p.budget").innerHTML = data.budget +" $ de budget";


        // article.querySelector("img").setAttribute("src", this.imageLien+this.largeurAffiche[2]+data.poster_path);

        if(data.overview === "")
        {
            article.querySelector("p.texteArticle").innerHTML = "Il n'y a pas de déscription pour le moment. " +
                "Elles sont faites par des bénévoles, si vous voulez aider la base de données de \"The Movie DataBase\"" +
                ". Merci!"
        }
        else{
            article.querySelector("p.texteArticle").innerHTML = data.overview;


        }

    }



    requeteActeur(id){

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteActeurActeurFilm.bind(this));

        xhr.open("GET", this.baseURL+"movie/"+id+"/credits?language="+this.lang+"&api_key="+this.APIkey);

        xhr.send();
    }

    retourRequeteActeur(e){

        let target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {


            data =  JSON.parse(target.responseText);

            this.afficherActeur(data);
        }
    }

    afficherActeur(data){

        for(let i = 0; i <data.cast.length;i++){


            if(data.cast[i].profile_path != null){
                let objet = document.createElement("img");
                objet.setAttribute("src", "http://image.tmdb.org/t/p/w200" + data.cast[i].profile_path);
                document.querySelector("#containActeur").appendChild(objet);
            }






        }

    }






}


