document.addEventListener("DOMContentLoaded", function () {



    //

    var connexion = new MovieDb();

    if(document.location.pathname.search("fiche-film.html")>0)
    {
        var params = (new URL(document.location) ).searchParams;

        connexion.requeteFilm(params.get("id"));
        connexion.requeteTopRated(params.get("id"));

    }
    else {
        connexion.getDernierFilm();
        connexion.requeteTopRated();

    }


    document.querySelector("#retour").addEventListener("click", top);

    function top() {
        window.scrollTo({top:0, behavior:'smooth'});
    }


    var Swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });




    document.querySelector("#connexion").addEventListener("click", function () {
        var modale = document.querySelector(".login-container-cache");

        modale.classList.add("login-container");
        modale.classList.remove("login-container-cache");

    });

    document.querySelector("#fermer").addEventListener("click", function () {
        var modale = document.querySelector(".login-container");

        modale.classList.remove("login-container");
        modale.classList.add("login-container-cache");

    });




});




function reduireTexte (element, nbrMaxMot, sufix) {


    // Nous vérifions si les arguments "element" et "maxChar"
    // sont bien passé dans l'appel de la fonction.
    if (!element || !nbrMaxMot) return;

    // Nous récupèrons le texte de l'élément
    var texte = element.textContent;

    // Nous convertisons la chaine de caractères en tableau avec .split(' ').
    // Le split utilise un espace, ce qui va donner un tableau
    // avec chaque mot dans une cellule.
    texte = texte.split(' ');

    // Nous arrêtons la fonction s'il y a moins de mots que nbrMaxMot
    if(texte.length < nbrMaxMot) return;

    // Nous coupons le tableau avec .slice(0, maxChar)
    // pour récupérer seulement le nombre de mots voulu.
    texte = texte.slice(0, nbrMaxMot);

    // Nous recréons une chaine de caractère avec .join(' ').
    // Le join utilise un espace pour séparer chaque mot.
    // S'il y a un sufix, nous ajoutons le sufix à la fin du texte.
    texte = texte.join(' ') + (sufix ? sufix : '');

    // Nous réinjectons le texte dans l'élément.
    element.textContent = texte;

};





class MovieDb {

    constructor() {

        this.APIkey = "34cca7090fdda59238f08b655f25faa7";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;

    }

    requeteTopRated() {

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteTopRated.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    retourRequeteTopRated(e) {

        let target = e.currentTarget;  //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficheTopRated(data);

            //console.log(data[2].title);
        }
    }

    afficheTopRated(data){

        for (var i = 0; i < this.totalFilm; i++){
            // console.log(data[i].title);

            let unArticle = document.querySelector(".template").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;

            if (data[i].overview == ""){
                unArticle.querySelector("p").innerText = "Pas de descriptions";
            }
            else{
                unArticle.querySelector("p").innerText = data[i].overview;
                reduireTexte(unArticle.querySelector("p"), 20, '...');
            }
            var date = unArticle.querySelector("#date").innerHTML = data[i].release_date;

            var uneImage = unArticle.querySelector("img").setAttribute("src",this.imgPath + "w" + this.largeurAffiche[5] + data[i].poster_path);

           var etoile = unArticle.querySelector("#etoile").innerHTML = data[i].vote_average/2 +"/5 Étoiles";



            document.querySelector(".liste-films").appendChild(unArticle);



        }
    }



    getDernierFilm(){

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourGetDernierFilm.bind(this));

        xhr.open("GET", this.baseURL+"movie/now_playing?language="+this.lang+"&api_key="+this.APIkey);

        xhr.send();
    }

    retourGetDernierFilm(e){

        let target = e.currentTarget;

        var data;

        if (target.readyState === target .DONE) {

            data =  JSON.parse(target.responseText).results;

            this.afficherDernierFilm(data);
        }
    }

    afficherDernierFilm(data){
        for (let i=0; i<9;i++) {
            // Clone
            let objet = document.querySelector("#toCloneSwiper").cloneNode(true);

            objet.setAttribute("href", "html/fiche-film.html?id="+data[i].id);
            objet.classList.remove("u-hidden");

            objet.querySelector("h3").innerHTML = data[i].title;
            objet.querySelector("p").innerHTML = data[i].vote_average/2 + " Étoiles sur 5";


            objet.querySelector("article").style.backgroundImage = "url('"+this.imageLien+"original"+data[i].backdrop_path+"')";




        }



    }





    requeteFilm(id){

        let xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteFilm.bind(this));

        xhr.open("GET", this.baseURL+"movie/"+id+"?language="+this.lang+"&api_key="+this.APIkey);

        xhr.send();
    }

    retourRequeteFilm(e){

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {


            data =  JSON.parse(target.responseText);

            this.afficherFilm(data);
        }
    }

    afficherFilm(data){

        console.log(data);

        document.querySelector("hero").style.backgroundImage = "url('"+this.imageLien+"original"+data.backdrop_path+"')";

        let article = document.querySelector("article");

        article.querySelector("h1").innerHTML = data.title;

        article.querySelector("p.date").innerHTML = data.release_date +" "+data.original_language;
        article.querySelector("p.etoile").innerHTML = data.vote_average/2 +"/5 Étoiles";

        article.querySelector("p.duree").innerHTML = data.runtime + " minutes";
        article.querySelector("p.budget").innerHTML = data.budget +" $ de budget";


        // article.querySelector("img").setAttribute("src", this.imageLien+this.largeurAffiche[2]+data.poster_path);

        if(data.overview === "")
        {
            article.querySelector("p.texteArticle").innerHTML = "Il n'y a pas de déscription pour le moment. " +
                "Elles sont faites par des bénévoles, si vous voulez aider la base de données de \"The Movie DataBase\"" +
                ". Merci!"
        }
        else{
            article.querySelector("p.texteArticle").innerHTML = data.overview;


        }

    }



    requeteActeur(id){

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteActeurActeurFilm.bind(this));

        xhr.open("GET", this.baseURL+"movie/"+id+"/credits?language="+this.lang+"&api_key="+this.APIkey);

        xhr.send();
    }

    retourRequeteActeur(e){

        let target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {


            data =  JSON.parse(target.responseText);

            this.afficherActeur(data);
        }
    }

    afficherActeur(data){

        for(let i = 0; i <data.cast.length;i++){


            if(data.cast[i].profile_path != null){
                let objet = document.createElement("img");
                objet.setAttribute("src", "http://image.tmdb.org/t/p/w200" + data.cast[i].profile_path);
                document.querySelector("#containActeur").appendChild(objet);
            }






        }

    }






}



//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xuXG5cblxuICAgIC8vXG5cbiAgICB2YXIgY29ubmV4aW9uID0gbmV3IE1vdmllRGIoKTtcblxuICAgIGlmKGRvY3VtZW50LmxvY2F0aW9uLnBhdGhuYW1lLnNlYXJjaChcImZpY2hlLWZpbG0uaHRtbFwiKT4wKVxuICAgIHtcbiAgICAgICAgdmFyIHBhcmFtcyA9IChuZXcgVVJMKGRvY3VtZW50LmxvY2F0aW9uKSApLnNlYXJjaFBhcmFtcztcblxuICAgICAgICBjb25uZXhpb24ucmVxdWV0ZUZpbG0ocGFyYW1zLmdldChcImlkXCIpKTtcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVUb3BSYXRlZChwYXJhbXMuZ2V0KFwiaWRcIikpO1xuXG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICBjb25uZXhpb24uZ2V0RGVybmllckZpbG0oKTtcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVUb3BSYXRlZCgpO1xuXG4gICAgfVxuXG5cbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3JldG91clwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdG9wKTtcblxuICAgIGZ1bmN0aW9uIHRvcCgpIHtcbiAgICAgICAgd2luZG93LnNjcm9sbFRvKHt0b3A6MCwgYmVoYXZpb3I6J3Ntb290aCd9KTtcbiAgICB9XG5cblxuICAgIHZhciBTd2lwZXIgPSBuZXcgU3dpcGVyKCcuc3dpcGVyLWNvbnRhaW5lcicsIHtcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMSxcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAzMCxcbiAgICAgICAgbG9vcDogdHJ1ZSxcbiAgICAgICAgbmF2aWdhdGlvbjoge1xuICAgICAgICAgICAgbmV4dEVsOiAnLnN3aXBlci1idXR0b24tbmV4dCcsXG4gICAgICAgICAgICBwcmV2RWw6ICcuc3dpcGVyLWJ1dHRvbi1wcmV2JyxcbiAgICAgICAgfSxcbiAgICB9KTtcblxuXG5cblxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY29ubmV4aW9uXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBtb2RhbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxvZ2luLWNvbnRhaW5lci1jYWNoZVwiKTtcblxuICAgICAgICBtb2RhbGUuY2xhc3NMaXN0LmFkZChcImxvZ2luLWNvbnRhaW5lclwiKTtcbiAgICAgICAgbW9kYWxlLmNsYXNzTGlzdC5yZW1vdmUoXCJsb2dpbi1jb250YWluZXItY2FjaGVcIik7XG5cbiAgICB9KTtcblxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjZmVybWVyXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBtb2RhbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxvZ2luLWNvbnRhaW5lclwiKTtcblxuICAgICAgICBtb2RhbGUuY2xhc3NMaXN0LnJlbW92ZShcImxvZ2luLWNvbnRhaW5lclwiKTtcbiAgICAgICAgbW9kYWxlLmNsYXNzTGlzdC5hZGQoXCJsb2dpbi1jb250YWluZXItY2FjaGVcIik7XG5cbiAgICB9KTtcblxuXG5cblxufSk7XG5cblxuXG5cbmZ1bmN0aW9uIHJlZHVpcmVUZXh0ZSAoZWxlbWVudCwgbmJyTWF4TW90LCBzdWZpeCkge1xuXG5cbiAgICAvLyBOb3VzIHbDqXJpZmlvbnMgc2kgbGVzIGFyZ3VtZW50cyBcImVsZW1lbnRcIiBldCBcIm1heENoYXJcIlxuICAgIC8vIHNvbnQgYmllbiBwYXNzw6kgZGFucyBsJ2FwcGVsIGRlIGxhIGZvbmN0aW9uLlxuICAgIGlmICghZWxlbWVudCB8fCAhbmJyTWF4TW90KSByZXR1cm47XG5cbiAgICAvLyBOb3VzIHLDqWN1cMOocm9ucyBsZSB0ZXh0ZSBkZSBsJ8OpbMOpbWVudFxuICAgIHZhciB0ZXh0ZSA9IGVsZW1lbnQudGV4dENvbnRlbnQ7XG5cbiAgICAvLyBOb3VzIGNvbnZlcnRpc29ucyBsYSBjaGFpbmUgZGUgY2FyYWN0w6hyZXMgZW4gdGFibGVhdSBhdmVjIC5zcGxpdCgnICcpLlxuICAgIC8vIExlIHNwbGl0IHV0aWxpc2UgdW4gZXNwYWNlLCBjZSBxdWkgdmEgZG9ubmVyIHVuIHRhYmxlYXVcbiAgICAvLyBhdmVjIGNoYXF1ZSBtb3QgZGFucyB1bmUgY2VsbHVsZS5cbiAgICB0ZXh0ZSA9IHRleHRlLnNwbGl0KCcgJyk7XG5cbiAgICAvLyBOb3VzIGFycsOqdG9ucyBsYSBmb25jdGlvbiBzJ2lsIHkgYSBtb2lucyBkZSBtb3RzIHF1ZSBuYnJNYXhNb3RcbiAgICBpZih0ZXh0ZS5sZW5ndGggPCBuYnJNYXhNb3QpIHJldHVybjtcblxuICAgIC8vIE5vdXMgY291cG9ucyBsZSB0YWJsZWF1IGF2ZWMgLnNsaWNlKDAsIG1heENoYXIpXG4gICAgLy8gcG91ciByw6ljdXDDqXJlciBzZXVsZW1lbnQgbGUgbm9tYnJlIGRlIG1vdHMgdm91bHUuXG4gICAgdGV4dGUgPSB0ZXh0ZS5zbGljZSgwLCBuYnJNYXhNb3QpO1xuXG4gICAgLy8gTm91cyByZWNyw6lvbnMgdW5lIGNoYWluZSBkZSBjYXJhY3TDqHJlIGF2ZWMgLmpvaW4oJyAnKS5cbiAgICAvLyBMZSBqb2luIHV0aWxpc2UgdW4gZXNwYWNlIHBvdXIgc8OpcGFyZXIgY2hhcXVlIG1vdC5cbiAgICAvLyBTJ2lsIHkgYSB1biBzdWZpeCwgbm91cyBham91dG9ucyBsZSBzdWZpeCDDoCBsYSBmaW4gZHUgdGV4dGUuXG4gICAgdGV4dGUgPSB0ZXh0ZS5qb2luKCcgJykgKyAoc3VmaXggPyBzdWZpeCA6ICcnKTtcblxuICAgIC8vIE5vdXMgcsOpaW5qZWN0b25zIGxlIHRleHRlIGRhbnMgbCfDqWzDqW1lbnQuXG4gICAgZWxlbWVudC50ZXh0Q29udGVudCA9IHRleHRlO1xuXG59O1xuXG5cblxuXG5cbmNsYXNzIE1vdmllRGIge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG5cbiAgICAgICAgdGhpcy5BUElrZXkgPSBcIjM0Y2NhNzA5MGZkZGE1OTIzOGYwOGI2NTVmMjVmYWE3XCI7XG5cbiAgICAgICAgdGhpcy5sYW5nID0gXCJmci1DQVwiO1xuXG4gICAgICAgIHRoaXMuYmFzZVVSTCA9IFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9cIjtcblxuICAgICAgICB0aGlzLmltZ1BhdGggPSBcImh0dHA6Ly9pbWFnZS50bWRiLm9yZy90L3AvXCI7XG5cbiAgICAgICAgdGhpcy5sYXJnZXVyQWZmaWNoZSA9IFtcIjkyXCIsIFwiMTU0XCIsIFwiMTg1XCIsIFwiMzQyXCIsIFwiNTAwXCIsIFwiNzgwXCJdO1xuXG4gICAgICAgIHRoaXMubGFyZ2V1clRldGVBZmZpY2hlID0gW1wiNDVcIiwgXCIxODVcIl07XG5cbiAgICAgICAgdGhpcy50b3RhbEZpbG0gPSA4O1xuXG4gICAgICAgIHRoaXMudG90YWxBY3RldXIgPSA2O1xuXG4gICAgfVxuXG4gICAgcmVxdWV0ZVRvcFJhdGVkKCkge1xuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICAgICAgLy94aHIud2l0aENyZWRlbnRpYWxzID0gdHJ1ZTtcblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJSZXF1ZXRlVG9wUmF0ZWQuYmluZCh0aGlzKSk7XG5cbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS9wb3B1bGFyP3BhZ2U9MSZsYW5ndWFnZT1cIiArIHRoaXMubGFuZyArIFwiJmFwaV9rZXk9XCIgKyB0aGlzLkFQSWtleSk7XG5cbiAgICAgICAgeGhyLnNlbmQoKTtcblxuICAgIH1cblxuICAgIHJldG91clJlcXVldGVUb3BSYXRlZChlKSB7XG5cbiAgICAgICAgbGV0IHRhcmdldCA9IGUuY3VycmVudFRhcmdldDsgIC8vWE1MSHR0cFJlcXVlc3RcbiAgICAgICAgbGV0IGRhdGE7XG5cbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xuICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UodGFyZ2V0LnJlc3BvbnNlVGV4dCkucmVzdWx0cztcbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZVRvcFJhdGVkKGRhdGEpO1xuXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGRhdGFbMl0udGl0bGUpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYWZmaWNoZVRvcFJhdGVkKGRhdGEpe1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy50b3RhbEZpbG07IGkrKyl7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhkYXRhW2ldLnRpdGxlKTtcblxuICAgICAgICAgICAgbGV0IHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudGVtcGxhdGVcIikuY2xvbmVOb2RlKHRydWUpO1xuXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImgyXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udGl0bGU7XG5cbiAgICAgICAgICAgIGlmIChkYXRhW2ldLm92ZXJ2aWV3ID09IFwiXCIpe1xuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicFwiKS5pbm5lclRleHQgPSBcIlBhcyBkZSBkZXNjcmlwdGlvbnNcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2V7XG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0ub3ZlcnZpZXc7XG4gICAgICAgICAgICAgICAgcmVkdWlyZVRleHRlKHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicFwiKSwgMjAsICcuLi4nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhciBkYXRlID0gdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIjZGF0ZVwiKS5pbm5lckhUTUwgPSBkYXRhW2ldLnJlbGVhc2VfZGF0ZTtcblxuICAgICAgICAgICAgdmFyIHVuZUltYWdlID0gdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsdGhpcy5pbWdQYXRoICsgXCJ3XCIgKyB0aGlzLmxhcmdldXJBZmZpY2hlWzVdICsgZGF0YVtpXS5wb3N0ZXJfcGF0aCk7XG5cbiAgICAgICAgICAgdmFyIGV0b2lsZSA9IHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiI2V0b2lsZVwiKS5pbm5lckhUTUwgPSBkYXRhW2ldLnZvdGVfYXZlcmFnZS8yICtcIi81IMOJdG9pbGVzXCI7XG5cblxuXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWZpbG1zXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XG5cblxuXG4gICAgICAgIH1cbiAgICB9XG5cblxuXG4gICAgZ2V0RGVybmllckZpbG0oKXtcblxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgIC8veGhyLndpdGhDcmVkZW50aWFscyA9IHRydWU7XG5cbiAgICAgICAgeGhyLmFkZEV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsIHRoaXMucmV0b3VyR2V0RGVybmllckZpbG0uYmluZCh0aGlzKSk7XG5cbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMK1wibW92aWUvbm93X3BsYXlpbmc/bGFuZ3VhZ2U9XCIrdGhpcy5sYW5nK1wiJmFwaV9rZXk9XCIrdGhpcy5BUElrZXkpO1xuXG4gICAgICAgIHhoci5zZW5kKCk7XG4gICAgfVxuXG4gICAgcmV0b3VyR2V0RGVybmllckZpbG0oZSl7XG5cbiAgICAgICAgbGV0IHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcblxuICAgICAgICB2YXIgZGF0YTtcblxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldCAuRE9ORSkge1xuXG4gICAgICAgICAgICBkYXRhID0gIEpTT04ucGFyc2UodGFyZ2V0LnJlc3BvbnNlVGV4dCkucmVzdWx0cztcblxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlckRlcm5pZXJGaWxtKGRhdGEpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYWZmaWNoZXJEZXJuaWVyRmlsbShkYXRhKXtcbiAgICAgICAgZm9yIChsZXQgaT0wOyBpPDk7aSsrKSB7XG4gICAgICAgICAgICAvLyBDbG9uZVxuICAgICAgICAgICAgbGV0IG9iamV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN0b0Nsb25lU3dpcGVyXCIpLmNsb25lTm9kZSh0cnVlKTtcblxuICAgICAgICAgICAgb2JqZXQuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCBcImh0bWwvZmljaGUtZmlsbS5odG1sP2lkPVwiK2RhdGFbaV0uaWQpO1xuICAgICAgICAgICAgb2JqZXQuY2xhc3NMaXN0LnJlbW92ZShcInUtaGlkZGVuXCIpO1xuXG4gICAgICAgICAgICBvYmpldC5xdWVyeVNlbGVjdG9yKFwiaDNcIikuaW5uZXJIVE1MID0gZGF0YVtpXS50aXRsZTtcbiAgICAgICAgICAgIG9iamV0LnF1ZXJ5U2VsZWN0b3IoXCJwXCIpLmlubmVySFRNTCA9IGRhdGFbaV0udm90ZV9hdmVyYWdlLzIgKyBcIiDDiXRvaWxlcyBzdXIgNVwiO1xuXG5cbiAgICAgICAgICAgIG9iamV0LnF1ZXJ5U2VsZWN0b3IoXCJhcnRpY2xlXCIpLnN0eWxlLmJhY2tncm91bmRJbWFnZSA9IFwidXJsKCdcIit0aGlzLmltYWdlTGllbitcIm9yaWdpbmFsXCIrZGF0YVtpXS5iYWNrZHJvcF9wYXRoK1wiJylcIjtcblxuXG5cblxuICAgICAgICB9XG5cblxuXG4gICAgfVxuXG5cblxuXG5cbiAgICByZXF1ZXRlRmlsbShpZCl7XG5cbiAgICAgICAgbGV0IHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgICAgICAvL3hoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xuXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91clJlcXVldGVGaWxtLmJpbmQodGhpcykpO1xuXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCtcIm1vdmllL1wiK2lkK1wiP2xhbmd1YWdlPVwiK3RoaXMubGFuZytcIiZhcGlfa2V5PVwiK3RoaXMuQVBJa2V5KTtcblxuICAgICAgICB4aHIuc2VuZCgpO1xuICAgIH1cblxuICAgIHJldG91clJlcXVldGVGaWxtKGUpe1xuXG4gICAgICAgIGxldCB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG5cbiAgICAgICAgbGV0IGRhdGE7XG5cbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xuXG5cbiAgICAgICAgICAgIGRhdGEgPSAgSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KTtcblxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlckZpbG0oZGF0YSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhZmZpY2hlckZpbG0oZGF0YSl7XG5cbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImhlcm9cIikuc3R5bGUuYmFja2dyb3VuZEltYWdlID0gXCJ1cmwoJ1wiK3RoaXMuaW1hZ2VMaWVuK1wib3JpZ2luYWxcIitkYXRhLmJhY2tkcm9wX3BhdGgrXCInKVwiO1xuXG4gICAgICAgIGxldCBhcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImFydGljbGVcIik7XG5cbiAgICAgICAgYXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaDFcIikuaW5uZXJIVE1MID0gZGF0YS50aXRsZTtcblxuICAgICAgICBhcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLmRhdGVcIikuaW5uZXJIVE1MID0gZGF0YS5yZWxlYXNlX2RhdGUgK1wiIFwiK2RhdGEub3JpZ2luYWxfbGFuZ3VhZ2U7XG4gICAgICAgIGFydGljbGUucXVlcnlTZWxlY3RvcihcInAuZXRvaWxlXCIpLmlubmVySFRNTCA9IGRhdGEudm90ZV9hdmVyYWdlLzIgK1wiLzUgw4l0b2lsZXNcIjtcblxuICAgICAgICBhcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLmR1cmVlXCIpLmlubmVySFRNTCA9IGRhdGEucnVudGltZSArIFwiIG1pbnV0ZXNcIjtcbiAgICAgICAgYXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicC5idWRnZXRcIikuaW5uZXJIVE1MID0gZGF0YS5idWRnZXQgK1wiICQgZGUgYnVkZ2V0XCI7XG5cblxuICAgICAgICAvLyBhcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1hZ2VMaWVuK3RoaXMubGFyZ2V1ckFmZmljaGVbMl0rZGF0YS5wb3N0ZXJfcGF0aCk7XG5cbiAgICAgICAgaWYoZGF0YS5vdmVydmlldyA9PT0gXCJcIilcbiAgICAgICAge1xuICAgICAgICAgICAgYXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicC50ZXh0ZUFydGljbGVcIikuaW5uZXJIVE1MID0gXCJJbCBuJ3kgYSBwYXMgZGUgZMOpc2NyaXB0aW9uIHBvdXIgbGUgbW9tZW50LiBcIiArXG4gICAgICAgICAgICAgICAgXCJFbGxlcyBzb250IGZhaXRlcyBwYXIgZGVzIGLDqW7DqXZvbGVzLCBzaSB2b3VzIHZvdWxleiBhaWRlciBsYSBiYXNlIGRlIGRvbm7DqWVzIGRlIFxcXCJUaGUgTW92aWUgRGF0YUJhc2VcXFwiXCIgK1xuICAgICAgICAgICAgICAgIFwiLiBNZXJjaSFcIlxuICAgICAgICB9XG4gICAgICAgIGVsc2V7XG4gICAgICAgICAgICBhcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLnRleHRlQXJ0aWNsZVwiKS5pbm5lckhUTUwgPSBkYXRhLm92ZXJ2aWV3O1xuXG5cbiAgICAgICAgfVxuXG4gICAgfVxuXG5cblxuICAgIHJlcXVldGVBY3RldXIoaWQpe1xuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICAgICAgLy94aHIud2l0aENyZWRlbnRpYWxzID0gdHJ1ZTtcblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJSZXF1ZXRlQWN0ZXVyQWN0ZXVyRmlsbS5iaW5kKHRoaXMpKTtcblxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwrXCJtb3ZpZS9cIitpZCtcIi9jcmVkaXRzP2xhbmd1YWdlPVwiK3RoaXMubGFuZytcIiZhcGlfa2V5PVwiK3RoaXMuQVBJa2V5KTtcblxuICAgICAgICB4aHIuc2VuZCgpO1xuICAgIH1cblxuICAgIHJldG91clJlcXVldGVBY3RldXIoZSl7XG5cbiAgICAgICAgbGV0IHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcblxuICAgICAgICB2YXIgZGF0YTtcblxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XG5cblxuICAgICAgICAgICAgZGF0YSA9ICBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpO1xuXG4gICAgICAgICAgICB0aGlzLmFmZmljaGVyQWN0ZXVyKGRhdGEpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYWZmaWNoZXJBY3RldXIoZGF0YSl7XG5cbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8ZGF0YS5jYXN0Lmxlbmd0aDtpKyspe1xuXG5cbiAgICAgICAgICAgIGlmKGRhdGEuY2FzdFtpXS5wcm9maWxlX3BhdGggIT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgbGV0IG9iamV0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKTtcbiAgICAgICAgICAgICAgICBvYmpldC5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgXCJodHRwOi8vaW1hZ2UudG1kYi5vcmcvdC9wL3cyMDBcIiArIGRhdGEuY2FzdFtpXS5wcm9maWxlX3BhdGgpO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY29udGFpbkFjdGV1clwiKS5hcHBlbmRDaGlsZChvYmpldCk7XG4gICAgICAgICAgICB9XG5cblxuXG5cblxuXG4gICAgICAgIH1cblxuICAgIH1cblxuXG5cblxuXG5cbn1cblxuXG4iXSwiZmlsZSI6InNjcmlwdC5qcyJ9
